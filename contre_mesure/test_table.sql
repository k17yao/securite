-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 02, 2020 at 08:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nom` text DEFAULT NULL,
  `prenom` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `pass` text DEFAULT NULL,
  `diplome` text DEFAULT NULL,
  `reve` text DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `pass`, `diplome`, `reve`, `date`) VALUES
(7, 'Merheb', 'Camil', 'mail_camil@gmail.com', 'camil', 'Ingénieur', 'Machine virtuelle marche', '2020-04-02 09:19:46'),
(8, '<script>alert(\"Faille XSS\");</script>', '<script>alert(\"Je vous ai bien eu\");</script>', '<script>alert(\"Vous êtes nuls\");</script>', 'mama', '<script>alert(\"base de donnée vulnérable\");</script>', '<script>alert(\"Trop facile\");</script>', '2020-04-02 09:45:21'),
(9, 'yao', 'marc arthur', 'mcarthur0711@yahoo.com', '$2y$10$5EiWiVC5XJ1h2iwy/y6TuO6Fta7euEtifn6/4RLqYpOAh0NxXpVC6', 'Master', 'Devenir Ninja', '2020-04-02 10:44:08'),
(10, 'test2', 'tata', '&lt;script&gt;alert(&quot;Vous êtes nuls&quot;);&lt;/script&gt;', '$2y$10$bOnvkBsjxtx66iLHjUsiSee60hRx8P1/J.essT956tj4pX4hhtzCO', 'Master', 'Nothing', '2020-04-02 10:48:36'),
(11, 'good ', 'user', 'user@gmail.com', '$2y$10$g.w2qdB1ZislEqAgOtswnuVk.FA0yjRMTtm.LXeYdJrLNPuH4eQ1.', 'Something', 'Nothing', '2020-04-02 11:09:26'),
(12, 'popo', 'popi', 'user2@gmail.com', '$2y$10$mdDM4voB91cFGJyNm7gpueb0rcSjv3eq6Ew/c2wKXkuZsr1ALLoIG', 'Ingénieur', 'ninja', '2020-04-02 11:47:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
