<?php
include("sql_connect.php");
// Contre mesure vulnérabilité XSS
$nom = htmlspecialchars($_POST["nom"]);
$prenom = htmlspecialchars($_POST["prenom"]);
$email = htmlspecialchars($_POST["email"]);
$pass_wd = htmlspecialchars($_POST["passwd"]);
$passwd_h = password_hash($pass_wd, PASSWORD_DEFAULT);
$diplome = htmlspecialchars($_POST["diplome"]);
$reve = htmlspecialchars($_POST["reve"]);

//vulnérabilités controle sur les champs
$nom = filter_var($nom, FILTER_SANITIZE_STRING);
$prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
$reve = filter_var($reve, FILTER_SANITIZE_STRING);
$test =true;
$error=[];

if(filter_var($email,FILTER_VALIDATE_EMAIL)===false){
    $test=false;
    $error[] = "email";
}
    

if($test){
    $req = $PDO->prepare("SELECT * from test.users where email LIKE :email;");
    $req->execute(array('email'=>$email));
    
    if($req->rowCount()){
        $success = 'exist_mail';
        $PDO=null;
        header("location: /register.php?success=".$success);
    }
    else{
        //contre mesure vulnérabilités SQLi (sql injection)
        $req = $PDO->prepare("INSERT INTO test.users (nom, prenom, email, pass, diplome, reve) values (:nom,:prenom,:email,:pass,:diplome,:reve);");
        $req->execute(array('nom'=>$nom, 'prenom'=>$prenom, 'email'=>$email, 'pass'=>$passwd_h, 'diplome'=>$diplome, 'reve'=>$reve));
    
        $success = 'created';
        $PDO=null;
        header("location: /index.php?success=".$success);
    }
}
else{
    $PDO=null;
    $error = implode(', ', $error);
    header("location: /register.php?error=".$error);
}




die();
?>