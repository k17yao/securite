<?php
include("sql_connect.php");

//contre mesure vulnérabilité à XSS
$data =array();
$email = htmlspecialchars($_POST["username"]);
$pass_wd = htmlspecialchars($_POST["passwd"]);

//contre mesure vulnérabilité à SQLi (sql injection)


$req = $PDO->prepare("SELECT * from test.users where email LIKE :email;");
$req->execute(array('email'=>$email));

if($req->rowCount()){
    
    while($result = $req->fetch(PDO::FETCH_ASSOC)){
        $data = $result;
        $pass_hash = $result["pass"];
    }
    if(!password_verify($pass_wd, $pass_hash)){
        $success = 'no_account';
        header("location: /index.php?success=".$success);
    }
    
}
else{
    $success = 'no_account';
    header("location: /index.php?success=".$success);
}


// closing connection
$PDO = null;
//echo 'username = '.$_GET["username"].' et password = '.$_GET["passwd"];
?>

<!DOCTYPE html>
<html>
<body>
        <div style="margin-left:25px">
            <h2><center>Profil :  </center></h2>
            <br><br>
            <hr>
            <h3>Welcome <?php echo $data["nom"]." ". $data["prenom"]; ?></h3>
            <br><br>
            <div>
                <label for="">Nom : </label>
                <span><?php echo $data["nom"]; ?></span>
            </div>
            <br>
            <div>
                <label for="">Prenom : </label>
                <span><?php echo $data["prenom"]; ?></span>
            </div>
            <br>
            <div>
                <label for="">email : </label>
                <span><?php echo $data["email"]; ?></span>
            </div>
            <br>
            <div>
                <label for="">Diplôme en cours : </label>
                <span><?php echo $data["diplome"]; ?></span>
            </div>
            <br>
            <div>
                <label for="">Ce dont tu rêves : </label>
                <span><?php echo $data["reve"]; ?></span>
            </div>
            <br>
            <div>
            <br><br>
            <hr>
            <p><b>Disconnect</b></p>
            <button onclick="window.location.href='/index.php'">Disconnect</button>
        </div>


</body>
</html>
