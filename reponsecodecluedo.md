

# CRITIQUE ET ANALYSE SÉCURITÉ DU CHALLENGE

**Réalisé par :**
* MERHEB Camil
* YAO Kassy Marc Arthur

## Exercice 1

**Analyse codecluedo_challenge_FINAL :**

1. Mot de passe écrit en dur dans le code source ⇒ victime d’un **social engeneering** si quelqu’un a accès à ce code source. Une solution est de crypter cette random_key ou utiliser un key management.

2. Il n’y a aucun contrôle sur les champs fournis (« ssn », « email » et « cmd »). Il ne faut pas faire confiance à l’utilisateur qui peut faire une **injection de code** (comme l’injection SQL, XSS et l’usurpation de droits). Les utilisateurs ont tous accès au répertoire « /tmp/ » et peuvent écrire dans le fichier sans aucune authentification préalable.
    * L’**usurpation de droit** peut se produire si l’utilisateur donne comme suffixe dans la méthode **secure_store** : **‘chaîne’,rx**. En faisant cela, il obtient les droits de lire le fichier et aussi de l'exécuter au lieu de juste écrire dans ce fichier. Cela peut être problématique du point de vu de la sécurité.

3. La clé est statique et donc avec plusieurs tentatives, on peut retrouver cette clé (**attaque par force brute**).

4. La méthode secure_store fait appel à la methode **f.open** pour créer un fichier en concaténant la chaine ‘**/tmp/**’ à ‘**/**’ et **suffixe**. Cette instruction créera forcément une erreur sur un système unix. Prenons l’exemple de **suffixe = ‘projet’** nous obtenons l'exécution de l’instruction **f.open(/temp//projet-...)** qui conduit inexorablement à une erreur. Cela semble certe une faute de syntaxe du point de vue du développement mais peut aussi impacter la sécurité globale de ce code, vu qu’il n’effectuera jamais complètement ses tâches.

