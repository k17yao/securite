
Exercice 2 (instructions pour l'exécution de l'application)
 

1- lancer la base de donnée mysql dans un container : 
 
Se rendre dans l'un des repertoires attack ou contre_mesure, puis exécuter ses commandes

-> docker run --name mysql_db -d -e MYSQL_ROOT_PASSWORD=504633 -e MYSQL_DATABASE=test mysql:5.7
-> mysql -h 172.17.0.2 -u root -p504633 test < test_table.sql

après ça la base de données se lance dans le container d'adresse IP 172.17.0.2

2 - Tester l'API vulnérable aux attaques SQL et XSS dans un container :

Se rendre dans le repertoire attack, puis exécuter ses commandes:

docker build -t attaque .
docker run -d -p 8080:80 -e HOST_DB=172.17.0.2 --name attaque attaque

On peut donc accéder aux services en tapant dans l'adresse du navigateur : http://172.17.0.3

A la fin du test, il faut arrêter les container en faisant :

docker kill attaque
docker rm attaque

3 - Tester l'API de contre mesure aux attaques SQL et XSS dans un container :

Se rendre dans le repertoire contre_mesure, puis exécuter ses commandes:

docker build -t secured .
docker run -d -p 8080:80 -e HOST_DB=172.17.0.2 --name secured secured

On peut donc accéder aux services en tapant dans l'adresse du navigateur : http://172.17.0.3

A la fin du test, il faut arrêter les container en faisant :

docker kill secured
docker rm secured

