# CRITIQUE ET ANALYSE SÉCURITÉ DU CHALLENGE

**Réalisé par :**
* MERHEB Camil
* YAO Kassy Marc Arthur

## Exercice 1

**Analyse codecluedo_challenge_FINAL :**

1. Mot de passe écrit en dur dans le code source ⇒ victime d’un **social engeneering** si quelqu’un a accès à ce code source. Une solution est de crypter cette random_key ou utiliser un key management.

2. Il n’y a aucun contrôle sur les champs fournis (« ssn », « email » et « cmd »). Il ne faut pas faire confiance à l’utilisateur qui peut faire une **injection de code** (comme l’injection SQL, XSS et l’usurpation de droits). Les utilisateurs ont tous accès au répertoire « /tmp/ » et peuvent écrire dans le fichier sans aucune authentification préalable.
    * L’**usurpation de droit** peut se produire si l’utilisateur donne comme suffixe dans la méthode **secure_store** : **‘chaîne’,rx**. En faisant cela, il obtient les droits de lire le fichier et aussi de l'exécuter au lieu de juste écrire dans ce fichier. Cela peut être problématique du point de vu de la sécurité.

3. La clé est statique et donc avec plusieurs tentatives, on peut retrouver cette clé (**attaque par force brute**).

4. La méthode secure_store fait appel à la methode **f.open** pour créer un fichier en concaténant la chaine ‘**/tmp/**’ à ‘**/**’ et **suffixe**. Cette instruction créera forcément une erreur sur un système unix. Prenons l’exemple de **suffixe = ‘projet’** nous obtenons l'exécution de l’instruction **f.open(/temp//projet-...)** qui conduit inexorablement à une erreur. Cela semble certe une faute de syntaxe du point de vue du développement mais peut aussi impacter la sécurité globale de ce code, vu qu’il n’effectuera jamais complètement ses tâches.
---
## Exercice 2

Nous avons acquis beaucoup de notions importantes concernant la sécurité relative au Web durant ce cours. À l'issu de ce projet, nous avons développé une API Web ayant des services interagissant avec une base de données. En effet, le démonstrateur que nous avons créé, se trouvant dans le document intitulé ***attack***, est sensible à plusieurs vulnérabilités comme l'injection SQL et XSS. D'autre part et afin de se protéger contre ces failles, nous avons conçu une version sécurisée se trouvant dans le dossier ***contre_mesure***, où nous avons aussi géré le contrôle des champs remplis par l'utilisateur.

1. SQLi

    Une injection SQL (SQLi) est un type d'exploitation d'une faille de sécurité d'une application (dans notre cas c’est un site Web) lors de l’authentification, interagissant avec une base de données. L'attaquant détourne les requêtes SQL en y injectant une chaîne non prévue par le développeur et pouvant compromettre la sécurité du système. Cette injection aura lieu dans les champs email ou password du site web.
    Une injection SQL peut être de la forme suivante :
    
    ```sql
    SELECT * FROM test.users WHERE email=' ' OR '1'='1' AND pass='' OR '1'='1' ;
    ```
    > Cette requête est utilisée avec des conditions qui sont toujours vraies (1=1) et va ainsi rendre à l’attaquant toutes les informations contenues dans la base de données.

    ```sql
    SELECT * FROM test.users WHERE email='EMAIL' AND pass=' ' OR '1'='1' --;
    ```
    > Cette requête permet à l’attaquant de se connecter sous n’importe quel utilisateur (en remplaçant EMAIL par le l’adresse mail de cet usager) ayant n’importe quel mot de passe. Nous avons toujours la condition vraie ('1'='1') et nous rend tout ce qui suit en commentaire à l’aide du symbole --. Le script va alors vérifier la condition si elle est vrai (ce qui est toujours le cas puisque l’attaquant l’a forcée) puis l’attaquant sera connecté sous la session de n’importe quel EMAIL existant dans la base de données.
    
    Suite à ces attaques, un utilisateur malveillant aura accès aux comptes des usagers (tous les comptes à travers la première requête alors qu’à un seul compte en utilisant la deuxième requête) et pourra s’authentifier par la suite en tant qu’un usager et d’apporter des changements à niveau du compte victime.

2. XSS

    Le Cross-site scripting (XSS) est un type de vulnérabilité de site web qui cible le code (script) d’une page web exécutée dans le navigateur de l’utilisateur en permettant aux attaquants d’injecter des scripts malveillants dans des pages web. En effet, les attaques XSS surviennent lorsque des sources non approuvées envoient des contenus malveillants aux navigateurs des utilisateurs en utilisant les vulnérabilités des sites web. Le code injecté en langage script sera alros interprété par le navigateur de la victime.
    Une attaque XSS peut être de la forme suivante :

    ```html
    "</input><script>alert(‘Il y a une faille XSS.’)</script>
    ```
    > Ce simple morceau de code injecté dans le champ username d’authentification affichera simplement un pop-up indiquant qu’il y a une faille XSS.
    
    ```html
    "</input><script>alert(document.cookie)</script>
    ```
    
    > Ci-dessus est une injection plus critique qui permet d’afficher les cookies contenant les identifiants des utilisateurs. Donc cette faille permet à un utilisateur malveillant de récupérer les valeurs des cookies.
    
    Donc une attaque XSS réussie aura de graves conséquences (comme l’exemple ci-dessus) qui permettra à un attaquant d’injecter un morceau de code qui lui retournera l’ensemble des identifiants des usagers du téléservice. Ensuite, l’attaquant pourra accéder au compte de n’importe quel utilisateur (en s’authentifiant à l’aide des cookies qu’il a récupéré) puis apporter des changements au compte de la victime.

3. Sanitisation des entrées (ou Contrôle des champs)

    Dans notre cas, on utilise les fonctions du langage PHP pour rendre sain les entrées de l'utilisateur. Il s'agit des fonctions **filter_var** pour s'assurer du type de la variable fourni, **htmlspecialchars** pour fixer toute attaque de type **XSS** et pou éviter l'**injection SQL**, on utilise les requêtes preparées du **pdo** de PHP.

## Résolution de l'exercice 1

    Voir le fichier reponsecodecluedo.md ou reponsecodecluedo.md.txt

---
# Sécurité Web

## Exercice 2 : instructions pour l'exécution de l'application

1. Lancer la base de donnée mysql dans un container :
    Se rendre dans l'un des répertoires ***attack*** ou ***contre_mesure***, puis exécuter les commandes suivantes.
    ```sh
    $ docker run --name mysql_db -d -e MYSQL_ROOT_PASSWORD=504633 -e MYSQL_DATABASE=test mysql:5.7
    $ mysql -h 172.17.0.2 -u root -p504633 test < test_table.sql
    ```
    Ensuite, la base de données se lance dans le container d'adresse IP *172.17.0.2*. Vous pourrez toujours vérifier l'adresse IP de votre container de base de donnée en faisant la commande:
    ```sh
    $ docker ps
    $ docker inspect ID_DU_CONTAINER | grep IPAddress
    ```

2. Tester l'API vulnérable aux attaques SQL et XSS dans un container :
    Se rendre dans le répertoire ***attack***, puis exécuter les commandes ci-dessous.
    ```sh
    $ docker build -t attaque .
    $ docker run -d -p 8080:80 -e HOST_DB=172.17.0.2 --name attaque attaque
    ```
    Nous pouvons donc accéder aux services en tapant dans l'adresse du navigateur : http://172.17.0.3
    À la fin du test, il faut arrêter les containers à travers les commandes suivantes :
    ```sh
    $ docker kill attaque
    $ docker rm attaque
    ```

3. Tester l'API de contre mesure aux attaques SQL et XSS dans un container :
    Se rendre dans le répertoire ***contre_mesure***, puis exécuter ces commandes :
    ```sh
    $ docker build -t secured .
    $ docker run -d -p 8080:80 -e HOST_DB=172.17.0.2 --name secured secured
    ```
    Nous pouvons donc accéder aux services en tapant dans l'adresse du navigateur : http://172.17.0.3
    
    À la fin du test, il faut arrêter les container en faisant :
    ```sh
    $ docker kill secured
    $ docker rm secured
    ```

