<!DOCTYPE html>
<html>
<body>
    <form action="/register_data.php" method="get">
        <div style="margin-left:25px">
            <h2><center>Time to register !</center></h2>
            <?php if(isset($_GET["success"]) && $_GET["success"]=="exist_mail"){
                echo "<span style='color:red'>Cette adresse est déjà enregistrée sur un autre compte.<br>Essayez une autre adresse email.<br></span>";
            } ?>
            <br>
            <div>
                <label for="nom">Nom * : </label>
                <input type="text" id="nom" name="nom" value="" required>
            </div>
            <br>
            <div>
                <label for="prenom">Prenom * : </label>
                <input type="text" id="prenom" name="prenom" value="" required>
            </div>
            <br>
            <div>
                <label for="email">email * : </label>
                <input type="text" id="email" name="email" value="" required>
            </div>
            <br>
            <div>
                <label for="passwd">Password * : </label>
                <input type="password" id="passwd" name="passwd" value="" required>
            </div>
            <br>
            <div>
                <label for="diplome">Quel diplome en cours : </label>
                <select id="diplome" name="diplome">
                    <option value="">selectionner diplome</option>
                    <option value="Ingénieur">Ingénieur</option>
                    <option value="Master">Master</option>
                </select>
                <!--<input type="text" id="diplome" name="diplome" value="" placeholder="Ingénieur or Master">-->
            </div>
            <br>
            <div>
                <label for="reve">Quel est ton rêve : </label>
                <input type="text" id="reve" name="reve" value="">
            </div>
            <br>
            <input type="submit" value="Finish">
        </div>
    </form>

</body>
</html>
